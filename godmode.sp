#include <sourcemod>

#pragma semicolon 1
#pragma newdecls required

Handle g_hGodCookie;


public void OnPluginStart()
{
	bool firstTime = true;
	bool godEnabled = false;
	g_hGodCookie = RegClientCookie("godmode_state", "Godmode state", CookieAccess_Protected);
	RegAdminCmd("sm_godmode", Command_Menu, 0, "Opens the adminge menu");
	RegAdminCmd("sm_god", Command_God, 0, "Enables God Mode");
	RegAdminCmd("sm_slayall" , Command_SlayAll, 0, "Slays all players except the user");
}

public Action Command_Menu(int client, int args)
{
	if (AreClientCookiesCached(client))
	{
		char sCookieValue[12];
		GetClientCookie(client, g_hGodCookie, sCookieValue, sizeof(sCookieValue));
		int cookieValue = StringToInt(sCookieValue);
		Menu menu = new Menu(Menu_Callback);
		
		if (cookieValue == 1){
			godEnabled = true;
			Command_God(client, true, false);
		}
		
		if (!godEnabled)
		{
			menu.AddItem("enable", "Enable God");
		}else{
			menu.AddItem("disable", "Disable God");
		}
		menu.AddItem("killPlayers", "Kill all players");
		menu.SetTitle("Adminge Menu: ");
		menu.Display(client, 30);
	}
	return Plugin_Handled;
}

public Action Command_SlayAll(int client)
{
	for (new i = 1; i<= MaxClients; i++){
		if(IsClientInGame(i) && !IsFakeClient(i) && i != client){
			kill_player(client, i);
		}
	}
	
	return Plugin_Handled;
}

public Action Command_God(int client, bool enable, bool first)
{
	if (enable){
		SetEntProp(client, Prop_Data, "m_takedamage", 0, 1);
		if (first){
			Command_SlayAll(client);
			PrintToChatAll("Not even anime can save you now.");
		}
		SetClientCookie(client, g_hGodCookie, "1")
	}else{
		SetEntProp(client, Prop_Data, "m_takedamage", 2, 1);
		SetClientCookie(client, g_hGodCookie, "2")
	}
	
	return Plugin_Handled;
}

kill_player(int client, int target)
{

	SDKHooks_TakeDamage(target, client, client, 9999.0);

}

public int Menu_Callback(Menu menu, MenuAction action, int param1, int param2)
{

	char t_name[64];
	switch(action){
		case MenuAction_Select:
		{
			char item[32];
			menu.GetItem(param2, item, sizeof(item));
			
			if(StrEqual(item, "enable")){
				Command_God(param1, true, firstTime);
				firstTime = false;
			}
			else if (StrEqual(item, "disable")){
				Command_God(param1, false, firstTime);
			}
			else if (StrEqual(item, "killPlayers")){
				Command_SlayAll(param1);
			}
		}
		case MenuAction_End:
		{
			delete menu;
		}
	}
}